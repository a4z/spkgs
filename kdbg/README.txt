#
####|-----handy-ruler----------------------------------------------------|
kdbg: kdbg
kdbg:
kdbg: KDbg is a graphical user interface to gdb, the GNU debugger.
kdbg: It provides an intuitive interface for setting breakpoints, 
kdbg: inspecting variables, and stepping through code.
kdbg: KDbg requires KDE, the K Desktop Environment, but you can of course 
kdbg: debug any program.
kdbg: 
kdbg: more at:
kdbg: http://www.kdbg.org
kdbg: 

