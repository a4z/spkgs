#
########|-----handy-ruler----------------------------------------------------|
fakeroot: fakeroot
fakeroot:
fakeroot: Fakeroot makes it possible to run commands in an environment
fakeroot: faking root privileges.  This is done by setting LD_PRELOAD
fakeroot: to libfakeroot.so, which provides wrappers around getuid, 
fakeroot: chown, chmod, mknod, stat, and so on, thereby creating a fake
fakeroot: root environment..
fakeroot: 
fakeroot: more at:
fakeroot: http://packages.debian.org/fakeroot
fakeroot: 

