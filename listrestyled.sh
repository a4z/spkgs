
echo "not restyled" > znotrestyled.txt
echo "restyled" > zrestyled.txt

for dname in `find . -type d -not -iwholename '*.hg*' -maxdepth 1`
do
  if [ "${dname}" != "./_restyle" ] && [ "${dname}" != "./experimental" ] && [ "${dname}" != "./_archive" ]; then 
    (
      source ${dname}/${dname}.SlackBuild
      if [[ -z "${RESTYLABLE}" ]] ; then
        echo ${dname}/${dname}.SlackBuild >> znotrestyled.txt
      else
        echo ${dname}/${dname}.SlackBuild >> zrestyled.txt
      fi
    )
  fi
  #echo $dname
done


