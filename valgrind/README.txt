#
########|-----handy-ruler----------------------------------------------------|
valgrind: valgrind 
valgrind:
valgrind: Valgrind is an award-winning suite of tools for debugging and
valgrind: profiling Linux programs.  With the tools that come with Valgrind,
valgrind: you can automatically detect many memory management and threading
valgrind: bugs, avoiding hours of frustrating bug-hunting, making your
valgrind: programs more stable.  You can also perform detailed profiling,
valgrind: to speed up and reduce memory use of your programs.
valgrind: more info at:
valgrind: http://valgrind.org/
valgrind: 

