#
###|-----handy-ruler----------------------------------------------------|
dmd: dmd
dmd: 
dmd: D is a language with C-like syntax and static typing.
dmd: It pragmatically combines efficiency, control, and modeling power,
dmd: with safety and programmer productivity.
dmd: 
dmd: This package contains the dmd compiler, some other tools and the 
dmd: phobos library.
dmd: more at:
dmd: http://dlang.org
dmd: 

