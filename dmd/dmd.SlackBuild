#!/bin/sh 

# this is a build script that creates a Slackware package
#    h a r a l d dot a c h i t z at g m a i l dot com 
#        no rights reserved                


makeBasicSetup(){

PRGNAM="dmd"
VERSION="2.065.0"

BUILD=${BUILD:-1}

TAG=${TAG:-_a4z}
CWD=$(pwd)

TMP=${TMP:-/tmp/a4zbuild}
PKG=${TMP}/package-${PRGNAM}-${VERSION} # don't kill existings if multiple versions builds, so name with version

OUTPUT=${OUTPUT:-$CWD}

SRCARCHIVEBASENAME=${PRGNAM}.${VERSION}
SRCARCHIVENAME=$SRCARCHIVEBASENAME.zip
SRCARCHIVELOC=${SRCARCHIVELOC:-$CWD} #where do I store the sources

BUILDDIR=${TMP}/dmd2

NUMJOBS=${NUMJOBS:--j2} #jobs for compiler 

}


doInfoSetup(){

PROGHOMEPAGE=http://dlang.org
SRCARCHIVDLBASE=http://downloads.dlang.org/releases/2014

}

writeDescriptionTo(){ #no need to explain that arg for this function is the slac-desc file

  if [ $# = 0  ] ; then 
	echo "slack-desc file name missing in doWriteDescTo()" ; exit 1
  fi


printf -vSPACER  "%${#PRGNAM}s" " "
SPACER=${SPACER// /#}

echo "#
${SPACER}|-----handy-ruler----------------------------------------------------|
${PRGNAM}: ${PRGNAM}
${PRGNAM}: 
${PRGNAM}: D is a language with C-like syntax and static typing.
${PRGNAM}: It pragmatically combines efficiency, control, and modeling power,
${PRGNAM}: with safety and programmer productivity.
${PRGNAM}: 
${PRGNAM}: This package contains the dmd compiler, some other tools and the 
${PRGNAM}: phobos library.
${PRGNAM}: more at:
${PRGNAM}: ${PROGHOMEPAGE}
${PRGNAM}: 
" > $1
}


doDownload(){

  wget -O ${SRCARCHIVELOC}/${SRCARCHIVENAME} ${SRCARCHIVDLBASE}/${SRCARCHIVENAME}
  
}


makeArchSetup(){

ARCH=${ARCH:-$(uname -m)}
if [ "$ARCH" = "i486" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i486 -mtune=i686"}
      LIBDIRSUFFIX=""
      DARCH=32
elif [ "$ARCH" = "i686" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i686 -mtune=i686"}
      LIBDIRSUFFIX=""
      DARCH=32
elif [ "$ARCH" = "x86_64" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -fPIC"}
      LIBDIRSUFFIX="64"
      DARCH=64
fi  
    
}

doCleanUp(){

rm -rf ${PKG}
rm -rf ${BUILDDIR}
  
}


doPrepareSource() {

mkdir -p ${TMP} ${PKG} ${OUTPUT}

cd ${TMP}

unzip ${SRCARCHIVELOC}/${SRCARCHIVENAME}

cd ${BUILDDIR}

chown -R root:root .
chmod -R u+w,go+r-w,a-s .


}

doMakeBuild(){

cd ${BUILDDIR}

  ( cd src/dmd
  sed -i "s|INSTALL_DIR=../../install|INSTALL_DIR=${BUILDDIR}/install|g" posix.mak 
    make -j ${NUMJOBS} -f posix.mak MODEL=${DARCH} 

  )

echo "#### dmd build complete, continue wiht druntime"


  ( cd src/druntime
    sed -i 's|DMD=../dmd/src/dmd|DMD?=dmd|g' posix.mak
    sed -i "s|INSTALL_DIR=../install|INSTALL_DIR=${BUILDDIR}/install|g" posix.mak
    PATH=$BUILDDIR/src/dmd:$PATH make -j ${NUMJOBS} -f posix.mak MODEL=${DARCH} 

  )
echo "#### druntime build complete, contineu with phobos"


  ( cd src/phobos 
    sed -i 's|DMD = ../dmd/src/dmd|DMD?=dmd|g' posix.mak
    sed -i "s|INSTALL_DIR = ../install|INSTALL_DIR=${BUILDDIR}/install|g" posix.mak
    PATH=$BUILDDIR/src/dmd:$PATH make -j ${NUMJOBS} -f posix.mak MODEL=${DARCH} 

  )   
echo "#### phobos build complete"


}

doMakeInstall(){
  
cd $BUILDDIR

DINCLUDEDIR=$PKG/usr/include/d

mkdir -p $DINCLUDEDIR
mkdir -p $PKG/usr/{lib${LIBDIRSUFFIX},bin} 

mkdir -p $DINCLUDEDIR/phobos
cp -a $BUILDDIR/src/druntime/import $DINCLUDEDIR/druntime
# do I realyneed 2 directories ? 
mkdir -p $DINCLUDEDIR/phobos
#pick only needed ones ...  
#cp -a $BUILDDIR/src/phobos/std $DINCLUDEDIR/phobos
#cp -a $BUILDDIR/src/phobos/etc $DINCLUDEDIR/phobos
# this should be all, crc is depredicated , rest is unit test and docu, but for now take all
cp -a $BUILDDIR/src/phobos/{*.d,etc,std} $DINCLUDEDIR/phobos


cp -a $BUILDDIR/man $PKG/usr

DOCDIR=$PKG/usr/doc/$PRGNAM-$VERSION
mkdir -p $DOCDIR/html
cp -a $BUILDDIR/html/d/* $DOCDIR/html
cp -a $BUILDDIR/html/dmlogo.gif $DOCDIR/html

cp $BUILDDIR/license.txt $DOCDIR/LICENSE
cp $BUILDDIR/src/druntime/LICENSE $DOCDIR//LICENSE_druntime

mkdir ${PKG}/etc
echo "[Environment]
DFLAGS=-I/usr/include/d/phobos -I/usr/include/d/druntime -L-L/usr/lib${LIBDIRSUFFIX} -L--no-warn-search-mismatch -L--export-dynamic
" > ${PKG}/etc/dmd.conf  

# no binaries so far, so everything can be set 
chmod -R go+r-w,a-s $PKG


# bin and libs now

install -m 0755 $BUILDDIR/linux/bin${DARCH}/ddemangle $PKG/usr/bin/ddemangle
install -m 0755 $BUILDDIR/linux/bin${DARCH}/dman $PKG/usr/bin/dman
install -m 0755 $BUILDDIR/linux/bin${DARCH}/dumpobj $PKG/usr/bin/dumpobj
install -m 0755 $BUILDDIR/linux/bin${DARCH}/obj2asm $PKG/usr/bin/obj2asm
install -m 0755 $BUILDDIR/linux/bin${DARCH}/rdmd $PKG/usr/bin/rdmd

install -m 0755 $BUILDDIR/src/dmd/dmd $PKG/usr/bin/dmd


install -m 0644 $BUILDDIR/src/druntime/lib/libdruntime-linux${DARCH}.a $PKG/usr/lib${LIBDIRSUFFIX}/libdruntime.a
install -m 0644 $BUILDDIR/src/phobos/generated/linux/release/${DARCH}/libphobos2.a $PKG/usr/lib${LIBDIRSUFFIX}/libphobos2.a  
  
}


doFixInstall(){

cd ${BUILDDIR}

( cd $PKG/usr/man
   find . -type f -exec gzip -9 {} \;
   for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
 )

cat ${CWD}/${PRGNAM}.SlackBuild > ${PKG}/usr/doc/${PRGNAM}-${VERSION}/${PRGNAM}.SlackBuild

mkdir -p ${PKG}/install
writeDescriptionTo ${PKG}/install/slack-desc
}


doPack(){
cd ${PKG}
/sbin/makepkg -l y -c n ${OUTPUT}/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz}
}

#define this so that restyle script will work
RESTYLABLE=1
#indent, otherwise sbostyler will not work!
  makeBasicSetup

# arch  & info setup here ? 
if [ $(basename $0) = ${PRGNAM}.SlackBuild ] ; then
  set -e #Exit immediately if a command exits with a non-zero status.
  makeArchSetup #note, order matters,
  doInfoSetup 
  
  if [ ! -f ${SRCARCHIVELOC}/${SRCARCHIVENAME} ]; then 
    doDownload
  fi
  
  doCleanUp
  doPrepareSource
  doMakeBuild
  doMakeInstall
  doFixInstall
  doPack
  doCleanUp
else 
  # TODO give a info that this is included ..
  echo ""
fi






