#
####|-----handy-ruler----------------------------------------------------|
lcov: lcov
lcov:
lcov: LCOV is a graphical front-end for GCC's coverage testing tool gcov.
lcov: It collects gcov data for multiple source files and creates HTML 
lcov: pages containing the source code annotated with coverage 
lcov: information.
lcov: It also adds overview pages for easy navigation within the file 
lcov: structure. LCOV supports statement, function and branch coverage 
lcov: measurement. More info at:
lcov: http://ltp.sourceforge.net/coverage/lcov.php
lcov: 

