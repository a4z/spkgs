#
################|-----handy-ruler----------------------------------------------------|
autoconf-archive: autoconf-archive
autoconf-archive:
autoconf-archive: The GNU Autoconf Archive is a collection of more than 450 macros
autoconf-archive: for GNU Autoconf that have been contributed as free software by 
autoconf-archive: friendly supporters of the cause from all over the Internet.
autoconf-archive: Every single one of those macros can be re-used without imposing
autoconf-archive: any restrictions whatsoever on the licensing of the generated
autoconf-archive: configure script. 
autoconf-archive: more at:
autoconf-archive: http://www.gnu.org/software/autoconf-archive
autoconf-archive: 

