#
#######|-----handy-ruler----------------------------------------------------|
zziplib: zziplib
zziplib:
zziplib: The zziplib library is intentionally lightweight, it offers the
zziplib: ability to easily extract data from files archived in a single zip
zziplib: file. Applications can bundle files into a single zip archive and
zziplib: access them. The implementation is based only on the (free) subset
zziplib: of compression with the  zlib algorithm which is actually used by 
zziplib: the zip/unzip tools.
zziplib: more at:
zziplib: http://zziplib.sourceforge.net/zzip-index.html
zziplib: 

