#/sbin/sh 


if [ $# = 0 ] ; then
  echo "no arguments given"
  echo "usage: sh "$(basename $0)" ../dirname [build arguments]
    where build arguments are:
    0 .. download the source if it does not exist
    1 .. extract source (and prepare / patch it)
    2 .. run the configure / make part
    3 .. run make install part
    4 .. run fix install part
    5 .. make the package
    
    build arguments must be in order and make sense
    if no build arguments are given, 1 2 3 4 5 will be the default.
    good luck!
  "

  exit 1
fi

_builddir=$(realpath $1)
_pkgname=$(basename $_builddir)

_inputscript=$_builddir/$_pkgname.SlackBuild
shift


if [[ ! -f ${_inputscript} ]]
then
  echo "can not find "${_inputscript}
  echo "usage: sh "$(basename $0)" ../dirname [build arguments]"
  exit 1
fi


# set defaults if nothing is given
if [ $# = 0 ] ; then
  _runsteps="1 2 3 4 5"
else
  _runsteps=$*
fi


set -e 

# current dir as source and output dir if nothing is set
OUTPUT=${OUTPUT:-$(pwd)}
SRCARCHIVELOC=${SRCARCHIVELOC:-$(pwd)} 

# now continue in the buildscript dir, required for all this action ..
# this could be avoided if it would be able to set CWD in the buildscript from here
# problem is the cat *.slackbuild to ... help , possible that can be changed..

_inputscript=$(realpath ${_inputscript})
cd ${_builddir}
source ${_inputscript}


makeArchSetup 


for _step in ${_runsteps} ; do

  case ${_step} in
    0)  doInfoSetup 
      if [ ! -f ${SRCARCHIVELOC}/${SRCARCHIVENAME} ]; then 
        doDownload
      fi
      ;;

    1)  doCleanUp
        doPrepareSource
        ;;

    2)  doMakeBuild
        ;;

    3)  doMakeInstall
        ;;

    4) doFixInstall
      ;;

    5) doPack
      ;;

    *) echo "error, unknown argument \"${_step}\", goodby!"
      exit 1
      ;;
  esac

done 



