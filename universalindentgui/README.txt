#
##################|-----handy-ruler----------------------------------------------------|
universalindentgui: universalindentgui
universalindentgui:
universalindentgui: A cross platform GUI for several code formatter, beautifier and 
universalindentgui: indenter like AStyle, GNU Indent, GreatCode, HTML Tidy , Uncrustify
universalindentgui: and many more.
universalindentgui: Main feature is a live preview to directly see how the selected 
universalindentgui: formatting option affects the source code.
universalindentgui:
universalindentgui: more at:
universalindentgui: http://universalindent.sourceforge.net
universalindentgui: 

