#!/bin/sh 

# this is a build script that creates a Slackware package
#    h a r a l d dot a c h i t z at g m a i l dot com 
#        no rights reserved                


makeBasicSetup(){

PRGNAM="fltk"
VERSION="1.3.3"

BUILD=${BUILD:-1}

TAG=${TAG:-_a4z}
CWD=$(pwd)

TMP=${TMP:-/tmp/a4zbuild}
PKG=${TMP}/package-${PRGNAM}-${VERSION} # don't kill existings if multiple versions builds, so name with version

OUTPUT=${OUTPUT:-$CWD}

SRCARCHIVEBASENAME=${PRGNAM}-${VERSION}-source
SRCARCHIVENAME=$SRCARCHIVEBASENAME.tar.gz
SRCARCHIVELOC=${SRCARCHIVELOC:-$CWD} #where do I store the sources

BUILDDIR=${TMP}/${PRGNAM}-${VERSION} 

NUMJOBS=${NUMJOBS:--j2} #jobs for compiler 

}


doInfoSetup(){

PROGHOMEPAGE=http://fltk.org/
SRCARCHIVDLBASE=http://fltk.org/pub/${PRGNAM}/${VERSION}/

}

writeDescriptionTo(){ #no need to explain that arg for this function is the slac-desc file

  if [ $# = 0  ] ; then 
	echo "slack-desc file name missing in doWriteDescTo()" ; exit 1
  fi


printf -vSPACER  "%${#PRGNAM}s" " "
SPACER=${SPACER// /#}

echo "#
${SPACER}|-----handy-ruler----------------------------------------------------|
${PRGNAM}: ${PRGNAM}
${PRGNAM}:
${PRGNAM}: 
${PRGNAM}: FLTK (pronounced fulltick) is a cross-platform C++ GUI toolkit.
${PRGNAM}: 
${PRGNAM}: 
${PRGNAM}: 
${PRGNAM}: 
${PRGNAM}: more at:
${PRGNAM}: ${PROGHOMEPAGE}
${PRGNAM}: 
" > $1
}


doDownload(){

  wget -O ${SRCARCHIVELOC}/${SRCARCHIVENAME} ${SRCARCHIVDLBASE}/${SRCARCHIVENAME}
  
}


makeArchSetup(){

ARCH=${ARCH:-$(uname -m)}
if [ "$ARCH" = "i486" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i486 -mtune=i686"}
      LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i686 -mtune=i686"}
      LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -fPIC"}
      LIBDIRSUFFIX="64"
fi  
    
}

doCleanUp(){

rm -rf ${PKG}
rm -rf ${BUILDDIR}
  
}


doPrepareSource() {

mkdir -p ${TMP} ${PKG} ${OUTPUT}

cd ${TMP}

tar xvf ${SRCARCHIVELOC}/${SRCARCHIVENAME}

cd ${BUILDDIR}

chown -R root:root .
chmod -R u+w,go+r-w,a-s .


}

doMakeBuild(){

cd ${BUILDDIR}

#use cmake, but leaf the configure call here

./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --enable-release \
  --disable-debug \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --enable-gl \
  --enable-largefile \
  --enable-shared \
  --enable-threads \
  --enable-xinerama \
  --enable-xft \
  --enable-xdbe \
  --enable-cairo \
  --build=$(uname -m)-slackware-linux \
  CFLAGS="$SLKCFLAGS" CXXFLAGS="$SLKCFLAGS"

# cmake ./ -DCMAKE_BUILD_TYPE=Release \
#  -DCMAKE_INSTALL_PREFIX=/usr \
#  -DOPTION_PREFIX_BIN=/usr/bin \
#  -DOPTION_PREFIX_LIB=/usr/lib${LIBDIRSUFFIX} \
#  -DOPTION_PREFIX_DOC=/usr/doc/$PRGNAM-$VERSION \
#  -DOPTION_PREFIX_MAN=/usr/man \
#  -DOPTION_BUILD_EXAMPLES=OFF \
#  -DOPTION_CAIRO=ON \
#  -DOPTION_CAIROEXT=ON \
#  -DOPTION_BUILD_SHARED_LIBS=ON \
#  -DOPTION_PREFIX_CONFIG=/usr/share/cmake/FLTK-1.3 \
#  -DCMAKE_EXE_LINKER_FLAGS=-ldl \
#  -DCMAKE_CXX_FLAGS_RELEASE="${SLKCFLAGS} -DNDEBUG"  \
#  -DCMAKE_C_FLAGS_RELEASE="${SLKCFLAGS} -DNDEBUG" 

 # OPTION_PREFIX_CONFIG where to install dif cmake files ....
 # ldl seems to be required int the cmake build ....

 #so no need to set them 
# OPTION_USE_GL - default ON
# OPTION_USE_THREADS - default ON
# OPTION_LARGE_FILE - default ON
# OPTION_USE_SYSTEM_LIBJPEG - default ON
# OPTION_USE_SYSTEM_ZLIB - default ON
# OPTION_USE_SYSTEM_LIBPNG - default ON
# OPTION_USE_XINERAMA - default ON
# OPTION_USE_XFT - default ON
# OPTION_USE_XDBE - default ON
 
 
make ${NUMJOBS} V=1

}

doMakeInstall(){
  
cd ${BUILDDIR}
#this workded with cmake
#make install/strip DESTDIR=${PKG}

#make install-strip DESTDIR=${PKG}  
make install DESTDIR=${PKG}
}


doFixInstall(){

cd ${BUILDDIR}

# if no games are build, remove man6 sudoku / blocks / checkers
(
  rm -rf ${PKG}/usr/man/man6 ${PKG}/usr/man/cat*
)

# no need for this file 
rm -rf ${PKG}/usr/include/FL/README.Xcode

( cd ${PKG}/usr/man
   find . -type f -exec gzip -9 {} \;
   for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
 )


mkdir -p ${PKG}/usr/doc/${PRGNAM}-${VERSION}

#replace this for current need
MDOCS="README"
cp -a ${MDOCS} ${PKG}/usr/doc/${PRGNAM}-${VERSION}
mv  ${PKG}/usr/share/doc/fltk/examples ${PKG}/usr/doc/${PRGNAM}-${VERSION}
rm -r ${PKG}/usr/share/

cat ${CWD}/${PRGNAM}.SlackBuild > ${PKG}/usr/doc/${PRGNAM}-${VERSION}/${PRGNAM}.SlackBuild

mkdir -p ${PKG}/install

writeDescriptionTo ${PKG}/install/slack-desc

}


doPack(){
cd ${PKG}
/sbin/makepkg -l y -c n ${OUTPUT}/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz}
}

#define this so that restyle script will work
RESTYLABLE=1
#indent, otherwise sbostyler will not work!
  makeBasicSetup

# arch  & info setup here ? 
if [ $(basename $0) = ${PRGNAM}.SlackBuild ] ; then
  set -e #Exit immediately if a command exits with a non-zero status.
  makeArchSetup #note, order matters,
  doInfoSetup 
  
  if [ ! -f ${SRCARCHIVELOC}/${SRCARCHIVENAME} ]; then 
    doDownload
  fi
  
  doCleanUp
  doPrepareSource
  doMakeBuild
  doMakeInstall
  doFixInstall
  doPack
  doCleanUp
else 
  # TODO give a info that this is included ..
  echo ""
fi






