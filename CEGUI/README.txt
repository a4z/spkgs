#
#####|-----handy-ruler----------------------------------------------------|
CEGUI: CEGUI
CEGUI:
CEGUI: Crazy Eddie's GUI System is a free library providing windowing and 
CEGUI: widgets for graphics APIs / engines where such functionality is not 
CEGUI: natively available, or severely lacking. The library is object 
CEGUI: orientated, written in C++, and targeted at games developers who 
CEGUI: should be spending their time creating great games, not building GUI
CEGUI: sub-systems!
CEGUI: more at
CEGUI: http://www.cegui.org.uk
CEGUI: 

