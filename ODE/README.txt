#
###|-----handy-ruler----------------------------------------------------|
ODE: ODE
ODE:
ODE: The Open Dynamics Engine is a free, industrial quality library
ODE: for simulating articulated rigid body dynamics. Proven applications
ODE: include simulating ground vehicles, legged creatures, and moving
ODE: objects in VR environments. It is fast, flexible and robust, and has
ODE: built-in collision detection.
ODE: 
ODE: more at:
ODE: http://www.ode.org
ODE: 

