#
#########|-----handy-ruler----------------------------------------------------|
nvidia-cg: NVIDIA Cg Toolkit including:
nvidia-cg: -NVIDIA Cg Compiler Release 2.1
nvidia-cg: -Cg/CgFX Runtime libraries for OpenGL
nvidia-cg: -Cg User's Manual / Documentation
nvidia-cg:   -Cg Language Specification -runtime APIs
nvidia-cg:   -Cg standard library
nvidia-cg:   -CgFX states -Cg profiles
nvidia-cg:   -Cg examples
nvidia-cg: more at:
nvidia-cg: https://developer.nvidia.com/cg-toolkit
nvidia-cg: 

