#
############|-----handy-ruler----------------------------------------------------|
MinimalBASIC: MinimalBASIC
MinimalBASIC:
MinimalBASIC: This software is a compiler for Minimal BASIC as specified by the
MinimalBASIC: ECMA-55 Minimal BASIC standard.
MinimalBASIC:
MinimalBASIC: The target is AMD64/EM64T/x86-64 machines running a modern Linux
MinimalBASIC: distribution.
MinimalBASIC:
MinimalBASIC: more at:
MinimalBASIC: http://buraphakit.sourceforge.net/BASIC.shtml
MinimalBASIC:

