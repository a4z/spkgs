#!/bin/sh 

# this is a build script that creates a Slackware package
#    h a r a l d dot a c h i t z at g m a i l dot com 
#        no rights reserved                


makeBasicSetup(){

PRGNAM="gperftools"
VERSION="2.1"

BUILD=${BUILD:-1}

TAG=${TAG:-_a4z}
CWD=$(pwd)

TMP=${TMP:-/tmp/a4zbuild}
PKG=${TMP}/package-${PRGNAM}-${VERSION} 

OUTPUT=${OUTPUT:-$CWD}

SRCARCHIVEBASENAME=${PRGNAM}-${VERSION}
SRCARCHIVENAME=$SRCARCHIVEBASENAME.tar.gz
SRCARCHIVELOC=${SRCARCHIVELOC:-$CWD} #where do I store the sources

BUILDDIR=${TMP}/${SRCARCHIVEBASENAME}

NUMJOBS=${NUMJOBS:--j2} #jobs for compiler 

}


doInfoSetup(){

PROGHOMEPAGE=http://code.google.com/p/gperftools
SRCARCHIVDLBASE=http://gperftools.googlecode.com/files

}

writeDescriptionTo(){ #no need to explain that arg for this function is the slac-desc file

  if [ $# = 0  ] ; then 
	echo "slack-desc file name missing in doWriteDescTo()" ; exit 1
  fi


printf -vSPACER  "%${#PRGNAM}s" " "
SPACER=${SPACER// /#}

echo "#
${SPACER}|-----handy-ruler----------------------------------------------------|
${PRGNAM}: ${PRGNAM}
${PRGNAM}:
${PRGNAM}: project formerly know as google-perftools
${PRGNAM}: These tools are for use by developers so that they can create more
${PRGNAM}: robust applications. Especially of use to those developing
${PRGNAM}: multi-threaded applications in C++ with templates.
${PRGNAM}: Includes TCMalloc, heap-checker, heap-profiler and cpu-profiler.
${PRGNAM}: 
${PRGNAM}: more at:
${PRGNAM}: $PROGHOMEPAGE
${PRGNAM}: 
" > $1
}


doDownload(){

  wget -O ${SRCARCHIVELOC}/${SRCARCHIVENAME} ${SRCARCHIVDLBASE}/${SRCARCHIVENAME}
  
}


makeArchSetup(){

ARCH=${ARCH:-$(uname -m)}
if [ "$ARCH" = "i486" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i486 -mtune=i686"}
      LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i686 -mtune=i686"}
      LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -fPIC"}
      LIBDIRSUFFIX="64"
fi  
    
}

doCleanUp(){

rm -rf ${PKG}
rm -rf ${BUILDDIR}
  
}


doPrepareSource() {

mkdir -p ${TMP} ${PKG} ${OUTPUT}

cd ${TMP}

tar xvf ${SRCARCHIVELOC}/${SRCARCHIVENAME}

cd ${BUILDDIR}

chown -R root:root .
chmod -R u+w,go+r-w,a-s .


}

doMakeBuild(){

cd ${BUILDDIR}

CFLAGS="${SLKCFLAGS}" CXXFLAGS="${SLKCFLAGS}" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --enable-release \
  --disable-debug \
  --build=$(uname -m)-slackware-linux \
  CFLAGS="$SLKCFLAGS" CXXFLAGS="$SLKCFLAGS"

make ${NUMJOBS}

}

doMakeInstall(){
  
cd ${BUILDDIR}

make install-strip DESTDIR=${PKG}
  
}


doFixInstall(){

cd ${BUILDDIR}

( cd $PKG/usr/man
   find . -type f -exec gzip -9 {} \;
   for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
)


mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/html

MDOCS="AUTHORS COPYING ChangeLog INSTALL NEWS README README_windows.txt TODO"
( cd $PKG/usr/share/doc/$PRGNAM-$VERSION
  cp -a $MDOCS $PKG/usr/doc/$PRGNAM-$VERSION
  rm $MDOCS
)
mv $PKG/usr/share/doc/$PRGNAM-$VERSION/* $PKG/usr/doc/$PRGNAM-$VERSION/html
rm -rf $PKG/usr/share


cat ${CWD}/${PRGNAM}.SlackBuild > ${PKG}/usr/doc/${PRGNAM}-${VERSION}/${PRGNAM}.SlackBuild

mkdir -p ${PKG}/install
writeDescriptionTo ${PKG}/install/slack-desc

}


doPack(){
cd ${PKG}
/sbin/makepkg -l y -c n ${OUTPUT}/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz}
}

#define this so that restyle script will work
RESTYLABLE=1
#indent, otherwise sbostyler will not work!
  makeBasicSetup

# arch  & info setup here ? 
if [ $(basename $0) = ${PRGNAM}.SlackBuild ] ; then
  set -e #Exit immediately if a command exits with a non-zero status.
  makeArchSetup #note, order matters,
  doInfoSetup 
  
  if [ ! -f ${SRCARCHIVELOC}/${SRCARCHIVENAME} ]; then 
    doDownload
  fi
  
  doCleanUp
  doPrepareSource
  doMakeBuild
  doMakeInstall
  doFixInstall
  doPack
  doCleanUp
else 
  # TODO give a info that this is included ..
  echo ""
fi






