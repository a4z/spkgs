#
#########|-----handy-ruler----------------------------------------------------|
libunwind: libunwind
libunwind:
libunwind: The primary goal of this project is to define a portable and
libunwind: efficient C programming interface (API) to determine the call-chain
libunwind: of a program. The API additionally provides the means to manipulate
libunwind: the preserved (callee-saved) state of each call-frame and to resume
libunwind: execution at any point in the call-chain (non-local goto).
libunwind: 
libunwind: more at:
libunwind: http://www.nongnu.org/libunwind/
libunwind: 

