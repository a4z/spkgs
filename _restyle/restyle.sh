




_pkgname=$(basename $1)
_inputscript=$1/${_pkgname}.SlackBuild

_outdir=${_pkgname}
_outscript=${_pkgname}/${_pkgname}.SlackBuild


if [[ ! -f ${_inputscript} ]]
then
  echo "can not find "${_inputscript}
  echo "usage: sh "$(basename $0)" ../dirname"
  exit 1
fi

source ${_inputscript}

if [[ -z "${RESTYLABLE}" ]] ; then
  echo  "\${RESTYLABLE} not set "${_inputscript}
  echo  "script may not in the new restylable format"
  echo " so I quit here"
  exit 1
fi

if [ "${_inputscript}" == "${_outscript}" ]; then
  echo "working directory confusion"
  echo "output script would override input script, bad."
  echo " so I quit here"
  exit 1  
fi


echo "restyle "${_inputscript}
echo "output will go to : "$(pwd)"/"${_pkgname}

if [ -d ${_pkgname} ] ; then
  echo "delete existing scipts in ./"${_pkgname}
  rm -rf ${_pkgname}/*
else
  mkdir ${_pkgname}
fi

#start a new file
echo "#!/bin/sh" > ${_outscript}


echo "#this is a slackbuild" >> ${_outscript}
echo "" >> ${_outscript}


echo "#basic setup" >> ${_outscript}
awk '/^}/{p=0};p;/^makeBasicSetup()/{p=1}' ${_inputscript} >> ${_outscript}

echo "#arch setup" >> ${_outscript}
awk '/^}/{p=0};p;/^makeArchSetup()/{p=1}' ${_inputscript} >> ${_outscript}

echo "set -e #Exit immediately if a command exits with a non-zero status." >> ${_outscript}
echo "" >> ${_outscript}

echo "#clean up" >> ${_outscript}
awk '/^}/{p=0};p;/^doCleanUp()/{p=1}' ${_inputscript} >> ${_outscript}


#makeBasicSetup
#makeUnpackComand does currently not work, postpone for later
#awk '/^}/{p=0};p;/^doPrepareSource()/{p=1}' ${_inputscript} | sed s/'${EXTRACTSOURCE}'/${EXTRACTSOURCE}/  >> ${_outscript}

echo "#prepare source" >> ${_outscript}
awk '/^}/{p=0};p;/^doPrepareSource()/{p=1}' ${_inputscript}  >> ${_outscript}

echo "#make build" >> ${_outscript} 
awk '/^}/{p=0};p;/^doMakeBuild()/{p=1}' ${_inputscript} >> ${_outscript}

echo "#install" >> ${_outscript} 
awk '/^}/{p=0};p;/^doMakeInstall()/{p=1}' ${_inputscript} >> ${_outscript}

echo "#fix install" >> ${_outscript} 
awk '/^}/{p=0};p;/^doFixInstall()/{p=1}' ${_inputscript} | sed '/writeDescriptionTo/d' >> ${_outscript}

# ok , hier das mit dem desc-file

doInfoSetup
writeDescriptionTo $(dirname ${_outscript})/slack-desc

echo "cat \$CWD/slack-desc > \$PKG/install/slack-desc" >> ${_outscript} 

echo "#pack" >> ${_outscript} 
awk '/^}/{p=0};p;/^doPack()/{p=1}' ${_inputscript} >> ${_outscript}


echo "restyle done, generated "${_outscript}

echo "start do some other stuff"



_infofile=$(dirname ${_outscript})/${PRGNAM}.info

echo "PRGNAM=\"${PRGNAM}\"" > ${_infofile}
echo "VERSION=\"${VERSION}\"" >> ${_infofile}
echo "HOMEPAGE=\"${PROGHOMEPAGE}\"" >> ${_infofile}

#set arch and do the arch setup to have the dl urls
ARCH="i686"
makeArchSetup
_dl32=${SRCARCHIVDLBASE}/${SRCARCHIVENAME}

ARCH="x86_64"
makeArchSetup
_dl64=${SRCARCHIVDLBASE}/${SRCARCHIVENAME}

echo "DOWNLOAD=\"${_dl32}\"" >> ${_infofile}
if [ -f ${_dl32} ]; then 
  echo "MD5SUM=\"$(md5sum ${_dl32})\"" >> ${_infofile}
else
  echo "MD5SUM=\"\"" >> ${_infofile}
fi

if [ ${_dl64} != ${_dl32} ] ; then 
  echo "DOWNLOAD_x86_64=\"${_dl64}\"" >> ${_infofile}
  if [ -f ${_dl64} ]; then 
    echo "MD5SUM_x86_64=\"$(md5sum ${_dl64})\"" >> ${_infofile}
  else
    echo "MD5SUM_x86_64=\"\"" >> ${_infofile}
  fi
else
  echo "DOWNLOAD_x86_64=\"\"" >> ${_infofile}
  echo "MD5SUM_x86_64=\"\"" >> ${_infofile}
fi

echo "REQUIRES=\"\"
MAINTAINER=\"\"
EMAIL=\"\""  >> ${_infofile}





