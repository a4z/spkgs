

for dname in `find ../ -type d -not -iwholename '*.hg*' -maxdepth 1`
do
  if [ "${dname}" != "./_restyle" ] && [ "${dname}" != "./experimental" ] && [ "${dname}" != "./_archive" ]; then 
    (
      sh restyle.sh ${dname} 
    )
  fi
  #echo $dname
done


