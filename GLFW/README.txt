#
####|-----handy-ruler----------------------------------------------------|
GLFW: GLFW
GLFW:
GLFW: GLFW is an Open Source, multi-platform library for creating windows
GLFW: with OpenGL contexts and managing input and events. It is easy to 
GLFW: integrate into existing applications and does not lay claim to the 
GLFW: main loop.
GLFW: This is glfw3, if you use glfw 2x read this:
GLFW: http://www.glfw.org/docs/3.0/moving.html
GLFW: 
GLFW: more info at: http://www.glfw.org
GLFW: this pkg can be used beside glfw 2.x since it builds libglfw3.a

