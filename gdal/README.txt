#
####|-----handy-ruler----------------------------------------------------|
gdal: gdal
gdal:
gdal: a translator library for raster geospatial data formats that is 
gdal: released by the Open Source Geospatial Foundation.
gdal: As a library, it presents a single abstract data model to the
gdal: calling application for all supported formats. It also comes with 
gdal: a variety of useful commandline utilities for data translation 
gdal: and processing.
gdal: more at:
gdal: http://www.gdal.org/
gdal: 

