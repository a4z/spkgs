#!/bin/sh

# this is a build script that creates a Slackware package
#    h a r a l d dot a c h i t z at g m a i l dot com
#        no rights reserved


makeBasicSetup(){

PRGNAM="fossil"
VERSION="1.31"

BUILD=${BUILD:-1}

TAG=${TAG:-_a4z}
CWD=$(pwd)

TMP=${TMP:-/tmp/a4zbuild}
PKG=${TMP}/package-${PRGNAM}-${VERSION} # don't kill existings if multiple versions builds, so name with version

OUTPUT=${OUTPUT:-$CWD}

SRCARCHIVEBASENAME=${PRGNAM}-src-201502231627
SRCARCHIVENAME=${SRCARCHIVEBASENAME}34.tar.gz
SRCARCHIVELOC=${SRCARCHIVELOC:-$CWD} #where do I store the sources

BUILDDIR=${TMP}/$SRCARCHIVEBASENAME #where to build

NUMJOBS=${NUMJOBS:--j2} #jobs for compiler

}


doInfoSetup(){

PROGHOMEPAGE=https://www.fossil-scm.org
SRCARCHIVDLBASE=https://www.fossil-scm.org/download

}

writeDescriptionTo(){ #no need to explain that arg for this function is the slac-desc file

  if [ $# = 0  ] ; then
	echo "slack-desc file name missing in doWriteDescTo()" ; exit 1
  fi


printf -vSPACER  "%${#PRGNAM}s" " "
SPACER=${SPACER// /#}
echo "#
${SPACER}|-----handy-ruler----------------------------------------------------|
${PRGNAM}: ${PRGNAM}
${PRGNAM}:
${PRGNAM}: Fossil,a simple, high-reliability, distributed software 
${PRGNAM}: configuration management includding 
${PRGNAM}: distributed version control, bug tracking, wiki, and technotes
${PRGNAM}: 
${PRGNAM}:
${PRGNAM}:
${PRGNAM}: more at:
${PRGNAM}: ${PROGHOMEPAGE}
${PRGNAM}:
" > $1
}

doDownload(){

echo https://www.fossil-scm.org/download/fossil-src-20150223162734.tar.gz
echo ${SRCARCHIVDLBASE}/${SRCARCHIVENAME}

  wget -O ${SRCARCHIVELOC}/${SRCARCHIVENAME} ${SRCARCHIVDLBASE}/${SRCARCHIVENAME}

}


makeArchSetup(){

ARCH=${ARCH:-$(uname -m)}
if [ "$ARCH" = "i486" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i486 -mtune=i686"}
      LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -march=i686 -mtune=i686"}
      LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
      SLKCFLAGS=${SLKCFLAGS:-"-O2 -fPIC"}
      LIBDIRSUFFIX="64"
fi

}

doCleanUp(){

rm -rf ${PKG}
rm -rf ${BUILDDIR}

}


doPrepareSource() {

mkdir -p ${TMP} ${PKG} ${OUTPUT}

cd ${TMP}

tar xvf ${SRCARCHIVELOC}/${SRCARCHIVENAME}

cd ${BUILDDIR}

chown -R root:root .
chmod -R u+w,go+r-w,a-s .


}

doMakeBuild(){

cd ${BUILDDIR}

./configure --prefix=/usr \
	--with-miniz \
	--with-th1-docs --with-th1-hooks \
	--json CFLAGS="${SLKCFLAGS}"

make ${NUMJOBS}


}

doMakeInstall(){

cd ${BUILDDIR}

make install DESTDIR=${PKG}

}


doFixInstall(){

cd ${BUILDDIR}

mkdir -p $PKG/usr/doc/${PRGNAM}-${VERSION}

cp COPYRIGHT-BSD2.txt \
  $PKG/usr/doc/${PRGNAM}-${VERSION}

cat ${CWD}/${PRGNAM}.SlackBuild > ${PKG}/usr/doc/${PRGNAM}-${VERSION}/${PRGNAM}.SlackBuild

mkdir -p ${PKG}/install
writeDescriptionTo ${PKG}/install/slack-desc
}


doPack(){
cd ${PKG}
/sbin/makepkg -l y -c n ${OUTPUT}/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz}
}

#define this so that restyle script will work
RESTYLABLE=1
#indent, otherwise sbostyler will not work!
  makeBasicSetup

# arch  & info setup here ?
if [ $(basename $0) = ${PRGNAM}.SlackBuild ] ; then
  set -e #Exit immediately if a command exits with a non-zero status.
  makeArchSetup #note, order matters,
  doInfoSetup

  if [ ! -f ${SRCARCHIVELOC}/${SRCARCHIVENAME} ]; then
    doDownload
  fi

  doCleanUp
  doPrepareSource
  doMakeBuild
  doMakeInstall
  doFixInstall
  doPack
  doCleanUp
else
  # TODO give a info that this is included ..
  echo ""
fi






